package com.example.preexac1;

public class ReciboNomina {

    private int numRecibo, puesto;
    private String nombre;
    private float horasTrabajoNormal, horasTrabajoExtras, ImpuestoPorc;

    public ReciboNomina(int numRecibo, int puesto, String nombre, float horasTrabajoNormal, float horasTrabajoExtras, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.puesto = puesto;
        this.nombre = nombre;
        this.horasTrabajoNormal = horasTrabajoNormal;
        this.horasTrabajoExtras = horasTrabajoExtras;
        ImpuestoPorc = impuestoPorc;
    }

    public ReciboNomina() {
        this.numRecibo = 0;
        this.puesto = 0;
        this.nombre = "";
        this.horasTrabajoNormal = 0.0f;
        this.horasTrabajoExtras = 0.0f;
        ImpuestoPorc = 0.0f;
    }



    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabajoNormal() {
        return horasTrabajoNormal;
    }

    public void setHorasTrabajoNormal(float horasTrabajoNormal) {
        this.horasTrabajoNormal = horasTrabajoNormal;
    }

    public float getHorasTrabajoExtras() {
        return horasTrabajoExtras;
    }

    public void setHorasTrabajoExtras(float horasTrabajoExtras) {
        this.horasTrabajoExtras = horasTrabajoExtras;
    }

    public float getImpuestoPorc() {
        return ImpuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        ImpuestoPorc = impuestoPorc;
    }

    public float calcularSubtotal(){
        float pagoBase = 200.0f;

        switch (puesto) {
            case 1:
                pagoBase += pagoBase * 0.20;
                break;
            case 2:
                pagoBase += pagoBase * 0.50;
                break;
            case 3:
                pagoBase += pagoBase * 1.00;
                break;
            default:

                break;
        }

        return ((pagoBase * horasTrabajoNormal) + (horasTrabajoExtras * pagoBase) *2);
    }

    public float calcularImpuesto(){
        ImpuestoPorc = calcularSubtotal();
        return (float) (ImpuestoPorc * 0.16);
    }

    public float calcularTotal() {
        float subtotal = calcularSubtotal();
        float impuesto = calcularImpuesto();
        return subtotal - impuesto;
    }
}
