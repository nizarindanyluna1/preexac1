package com.example.preexac1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboNominaActivity extends AppCompatActivity {

    private TextView txtNombre;
    private EditText txtNumeroR, txtNombre2, txtHTrabajadas, txtHExtras, txtSubtotal, txtImpuesto, txtTotal;
    private Button btnSalir, btnCalcular, btnLimpiar;
    private RadioButton radioAux, radioAlbañil, radioObra;
    private RadioGroup grupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);
        iniciarComponentes();

        Intent intent = getIntent();
        String cliente = intent.getStringExtra("cliente");

        if (cliente != null) {
            txtNombre.setText(cliente);
        } else {
            Toast.makeText(ReciboNominaActivity.this, "Usuario no capturado", Toast.LENGTH_SHORT).show();
        }

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtHTrabajadas.getText().toString().matches("") ||
                        txtHExtras.getText().toString().matches("") ||
                        grupo.getCheckedRadioButtonId() == -1 ||
                        txtNumeroR.getText().toString().matches("") ||
                        txtNombre2.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(),"Agregue todos los datos", Toast.LENGTH_SHORT).show();
                }
                else{
                    calcular();
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNumeroR.setText("");
                txtNombre2.setText("");
                txtHExtras.setText("");
                txtHTrabajadas.setText("");
                txtSubtotal.setText("");
                txtImpuesto.setText("");
                txtTotal.setText("");
                grupo.clearCheck();
            }
        });

    }

    public void calcular(){
        int numRecibo = Integer.parseInt(txtNumeroR.getText().toString());
        String nombre = txtNombre.getText().toString();
        float horasTNormal = Float.parseFloat(txtHTrabajadas.getText().toString());
        float horasTrabExtras = Float.parseFloat(txtHExtras.getText().toString());
        int puesto = 0;

        if(grupo.getCheckedRadioButtonId() == R.id.radioAux){
            puesto = 1;
        }
        else if(grupo.getCheckedRadioButtonId() == R.id.radioAlbañil){
            puesto = 2;
        }
        else if(grupo.getCheckedRadioButtonId() == R.id.radioObra){
            puesto = 3;
        }
        float impuestoPorc = 16.0f; // Porcentaje de impuesto fijo

        // Crear una instancia de Recibo
        ReciboNomina recibo = new ReciboNomina(numRecibo, puesto, nombre, horasTNormal, horasTrabExtras, impuestoPorc);

        // Calcular subtotal, impuesto y total
        float subtotal = recibo.calcularSubtotal();
        float impuesto = recibo.calcularImpuesto();
        float total = recibo.calcularTotal();

        // Mostrar los resultados en la interfaz de usuario
        txtSubtotal.setText("$" + subtotal);
        txtImpuesto.setText("$" + impuesto);
        txtTotal.setText("$" + total);

}

    public void iniciarComponentes(){
        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtNumeroR = (EditText) findViewById(R.id.txtNumeroR);
        txtNombre2= (EditText) findViewById(R.id.txtNombre2);
        txtHTrabajadas = (EditText) findViewById(R.id.txtHTrabajadas);
        txtHExtras = (EditText) findViewById(R.id.txtHExtras);
        txtSubtotal = (EditText) findViewById(R.id.txtSubtotal);
        txtImpuesto = (EditText) findViewById(R.id.txtImpuesto);
        txtTotal = (EditText) findViewById(R.id.txtTotal);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnSalir = (Button) findViewById(R.id.btnSalir);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        radioAlbañil = (RadioButton) findViewById(R.id.radioAlbañil);
        radioAux = (RadioButton) findViewById(R.id.radioAux);
        radioObra = (RadioButton) findViewById(R.id.radioObra);
        grupo = (RadioGroup) findViewById(R.id.grupo);
    }
}